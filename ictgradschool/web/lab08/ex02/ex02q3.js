"use strict";

var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// TODO Your code here.
// write a program which prints the average of all the elements in the given array:

var sumTotal = 0;

for (var i = 0; i < numbers.length; i++) {
    sumTotal += numbers[i]; 
}

console.log("The average of all numbers in the array is: " + (sumTotal/numbers.length));

