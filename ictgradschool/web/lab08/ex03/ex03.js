"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    return Math.round(number * 100) / 100;
}

// TODO Write a function which calculates and returns the volume of a cone.
// V = pie r sq (h / 3)
function calcConeVol(height, radius) {
    return (Math.PI * (Math.pow(radius, 2))) * (height/3);
}

// TODO Write a function which calculates and returns the volume of a cylinder.
// V = pie r sq height
function calCylinderVol(height, radius) {
    return (Math.PI * (Math.pow(radius, 2))) * height;
}

// TODO Write a function which prints the name and volume of a shape, to 2dp.
// The volume of the <name> is: <volume> cm^3
function printShapeNameAndVol(name, volume) {
    console.log("The volume of the " + name + " is: " + roundTo2dp(volume) + " cm^3");
}

// ------------------------------------------

//exercise4
// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.

var coneRadius = getRndInteger(25, 50);
var coneHeight = getRndInteger(25, 50);

var cylinderRadius = getRndInteger(25, 50);
var cylinderHeight = getRndInteger(25, 50);

var coneVolume = calcConeVol(coneHeight, coneRadius);
var cylinderVolume = calCylinderVol(cylinderHeight, cylinderRadius);

printShapeNameAndVol("cone", coneVolume);
printShapeNameAndVol("cylinder", cylinderVolume);

var maxVol = Math.max(coneVolume, cylinderVolume);

var maxVolShapeName = "";

if (maxVol == coneVolume) {
    maxVolShapeName = "cone";
} else {
    maxVolShapeName = "cylinder";
}

console.log("The shape with the largest volume is " + maxVolShapeName + ", with a volume of " + roundTo2dp(maxVol) + " cm^3");
