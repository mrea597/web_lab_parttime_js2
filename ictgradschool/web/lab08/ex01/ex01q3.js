"use strict";

// calculate average of all even numbers from 1-20 (inclusive) using a for-loop:
var total = 0;
var count = 0;
for (var i = 2; i <= 20; i+=2) {
    total += i;
    count++;
}
console.log(total/count);


