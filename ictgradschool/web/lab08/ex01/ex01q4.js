"use strict";

//write a program that prints all the factorials from 1! to 10!
// e.g. 3! = 3 x 2 x 1 (6)

var number = 1;
while (number <= 10) {
    var factorialTotal = 1;

    for (var i = 1; i < number; i++) {
        factorialTotal *= (i+1);
    }
    console.log(number + "! = " + factorialTotal);
    number++;
}


