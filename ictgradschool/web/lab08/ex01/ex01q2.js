"use strict";
//exercise one: 2:
//print average of all even numbers between 1 & 20 (inclusive) using while loop

var number = 1;
var evenNumCount = 0;
var total = 0;
while (number <= 20) {
    if (number % 2 == 0) {
        total += number;
        evenNumCount++;
    }
    number++;
}
console.log(total/evenNumCount);
