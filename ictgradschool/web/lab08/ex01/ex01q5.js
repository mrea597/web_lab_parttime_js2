"use strict";

// Provided variable
var theString = "Hello World!";

// Variable you'll be modifying
var reversed = "";

// TODO Your answer here.
var strLen = theString.length;
for (var i = 1; i <= strLen; i++) {
    reversed += theString.charAt(strLen-i);
}

// Printing the answer.
console.log("\"" + theString + "\", reversed, is: \"" + reversed + "\".");

var numbers = [-9, 2, 7, 5, 124, -5, 1, 144];